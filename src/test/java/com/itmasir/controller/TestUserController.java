package com.itmasir.controller;

import com.itmasir.Application;
import com.itmasir.domain.User;
import com.itmasir.property.JDBCProperties;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * @author shinian
 * @version V1.0
 * @Description: TODO
 * @date 2017/12/28/028 21:54
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class TestUserController {

    @Autowired
    private UserController userController;

    @Autowired
    private JDBCProperties jdbcProperties;
    @Test
    public void test(){
        System.out.println(jdbcProperties);
    }

    @Test
    public void findAll(){
        List<User> userList = userController.getUserList();
        for (User user : userList) {
            System.out.println(user);
        }
    }

    @Test
    public void add(){
        User user = new User();
        user.setId(1L);
        user.setName("shinian");
        user.setAge(20);

        String returnValue = userController.postUser(user);

        System.out.println(returnValue);
    }



}
