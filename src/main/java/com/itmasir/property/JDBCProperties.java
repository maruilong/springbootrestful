package com.itmasir.property;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author shinian
 * @version V1.0
 * @Description: TODO
 * @date 2017/12/28/028 22:31
 */
@Component
@ConfigurationProperties("spring.datasource")
public class JDBCProperties {

    private String url;

    private String username;

    private String password;

    private String driverClassName;

    private Integer initPoolSize;

    private Integer maxPoolSize;

    private Integer minPoolSize;

    private Integer appendPoolSize;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDriverClassName() {
        return driverClassName;
    }

    public void setDriverClassName(String driverClassName) {
        this.driverClassName = driverClassName;
    }

    public Integer getInitPoolSize() {
        return initPoolSize;
    }

    public void setInitPoolSize(Integer initPoolSize) {
        this.initPoolSize = initPoolSize;
    }

    public Integer getMaxPoolSize() {
        return maxPoolSize;
    }

    public void setMaxPoolSize(Integer maxPoolSize) {
        this.maxPoolSize = maxPoolSize;
    }

    public Integer getMinPoolSize() {
        return minPoolSize;
    }

    public void setMinPoolSize(Integer minPoolSize) {
        this.minPoolSize = minPoolSize;
    }

    public Integer getAppendPoolSize() {
        return appendPoolSize;
    }

    public void setAppendPoolSize(Integer appendPoolSize) {
        this.appendPoolSize = appendPoolSize;
    }

    @Override
    public String toString() {
        return "JDBCProperties{" +
                "url='" + url + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", driverClassName='" + driverClassName + '\'' +
                ", initPoolSize=" + initPoolSize +
                ", maxPoolSize=" + maxPoolSize +
                ", minPoolSize=" + minPoolSize +
                ", appendPoolSize=" + appendPoolSize +
                '}';
    }
}
