package com.itmasir.controller;

import com.itmasir.domain.City;
import com.itmasir.service.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author shinian
 * @version V1.0
 * @Description: TODO
 * @date 2017/12/28/028 23:56
 */
@RestController
@RequestMapping("/city")
public class CityController extends AbstractController {

    @Autowired
    private CityService cityService;

    @RequestMapping(value = "/" ,method = {RequestMethod.POST})
    public String save(@ModelAttribute City city){
        if (cityService.save(city)){
            return SUCCESS;
        }else{
            return ERROR;
        }
    }
}
