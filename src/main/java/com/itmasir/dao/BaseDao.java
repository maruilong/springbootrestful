package com.itmasir.dao;

import java.io.Serializable;
import java.util.List;

/**
 * @author shinian
 * @version V1.0
 * @Description: TODO
 * @date 2017/12/28/028 22:35
 */
public interface BaseDao<T> {

    boolean save(T t);

    T update(T t);

    List<T> findAll();

    void delete(Serializable id);
}
