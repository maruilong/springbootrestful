package com.itmasir.dao.impl;

import com.itmasir.dao.CityDao;
import com.itmasir.db.DataSource;
import com.itmasir.domain.City;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

/**
 * @author shinian
 * @version V1.0
 * @Description: TODO
 * @date 2017/12/28/028 22:36
 */
@Repository
public class CityDaoImpl implements CityDao {

    @Autowired
    private DataSource dataSource;

    @Override
    public boolean save(City city) {

        String sql = "insert into city values(null,?,?,?)";

        Connection connection = dataSource.getAutoCommitConnection();
        try {

            PreparedStatement preparedStatement = connection.prepareStatement(sql);

            preparedStatement.setObject(1,city.getProvinceId());
            preparedStatement.setObject(2,city.getCityName());
            preparedStatement.setObject(3,city.getDescription());

            int count = preparedStatement.executeUpdate();

            return count>0;

        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            dataSource.revert(connection);
        }

        return false;
    }

    @Override
    public City update(City city) {
        return null;
    }

    @Override
    public List<City> findAll() {
        return null;
    }

    @Override
    public void delete(Serializable id) {

    }
}
