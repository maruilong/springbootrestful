package com.itmasir.db;

import java.sql.Connection;

/**
 * @author shinian
 * @version V1.0
 * @Description: TODO
 * @date 2017/12/28/028 22:42
 */
public interface DataSource {

    Connection getConnection();

    Connection getAutoCommitConnection();

    void revert(Connection connection);
}
