package com.itmasir.db;

import com.itmasir.property.JDBCProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.LinkedList;

/**
 * @author shinian
 * @version V1.0
 * @Description: TODO
 * @date 2017/12/28/028 22:42
 */
@Component
public class MyDataSource implements DataSource {

    private JDBCProperties jdbcProperties;

    private LinkedList<Connection> dataSources;

    public MyDataSource()  {
        dataSources = new LinkedList<>();
    }

    public void init(){
        dataSources.addAll(initConnections(jdbcProperties.getInitPoolSize()));
    }

    @Override
    public Connection getConnection() {

        System.out.println(dataSources.size());

        checkQuantity();
        final Connection connection =dataSources.removeFirst();
        try {
            connection.setAutoCommit(false);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }

    @Override
    public Connection getAutoCommitConnection() {
        checkQuantity();
        Connection connection =   getConnection();
        try {
            connection.setAutoCommit(true);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }

    public void checkQuantity(){

        if (dataSources.size() < jdbcProperties.getMinPoolSize()){
            append(jdbcProperties.getAppendPoolSize());
        }
    }

    public void append(int appendPoolSize){

        dataSources.addAll(initConnections(appendPoolSize));

    }

   private LinkedList<Connection> initConnections(int quantity){
        LinkedList<Connection> linkedList = new LinkedList<>();

        for (int i = 0; i < quantity; i++) {

            try {
                Class.forName(jdbcProperties.getDriverClassName());
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }

            Connection connection = null;
            try {
                connection = DriverManager.getConnection(jdbcProperties.getUrl(), jdbcProperties.getUsername(), jdbcProperties.getPassword());
            } catch (SQLException e) {
                e.printStackTrace();
            }

            linkedList.add(connection);
        }

        return  linkedList;
    }

    @Override
    public void revert(Connection connection){
        if (dataSources.size() > jdbcProperties.getMaxPoolSize()){
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        dataSources.add(connection);
    }

    @Autowired
    public void setJdbcProperties(JDBCProperties jdbcProperties) {
        this.jdbcProperties = jdbcProperties;
        init();
    }
}
