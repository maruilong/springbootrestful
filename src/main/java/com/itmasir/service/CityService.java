package com.itmasir.service;

import com.itmasir.domain.City;

/**
 * @author shinian
 * @version V1.0
 * @Description: TODO
 * @date 2017/12/28/028 23:54
 */
public interface CityService {

    public boolean save(City city);
}
