package com.itmasir.service.impl;

import com.itmasir.dao.CityDao;
import com.itmasir.domain.City;
import com.itmasir.service.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author shinian
 * @version V1.0
 * @Description: TODO
 * @date 2017/12/28/028 23:55
 */

@Service
public class CityServiceImpl implements CityService {

    @Autowired
    private CityDao cityDao;

    @Override
    public boolean save(City city) {
        return cityDao.save(city);
    }
}
